# Praktikum 1 Sistem Operasi 
Perkenalkan kami dari kelas ``Sistem Operasi B Kelompok  B08``, dengan Anggota sebagai berikut:

| Nama                      | NRP        |
|---------------------------|------------|
|Dimas Prihady Setyawan     | 5025211184 |
|Yusuf Hasan Nazila         | 5025211255 |
|Ahda Filza Ghaffaru        | 5025211144 |

# Penjelasan soal nomor 1
Untuk nomor 1, kita perlu mengolah suatu pemeringkatan dari sejumlah universitas di dunia berdasarkan file ``2023 QS World University Rankings.csv`` yang telah diberikan. Terdapat 4 permasalahan yang perlu diselesaikan, berikut adalah langkah-langkahnya.

### Langkah Poin 1
Tampilkan Top 5 Universitas di Jepang. Dikarenakan kita akan membaca file csv per-barisnya, maka perlu digunakan command/peritah *awk* pada script ``university_survey.sh``. Selection criteria yang perlu dilakukan adalah jika n < 5 dan baris mengandung kata kunci "Japan", maka lakukan proses print lalu increment n. 
```
awk '{
    if((n<5) && (/Japan)){
        print
        ++n
    }
}' "2023 QS World University Rankings.csv"
```
Dapat dilihat bahwa variabel n disini berperan sebagai batas dari baris yang akan di print yaitu 5.

### Langkah Poin 2
Dari output poin pertama, tampilkan Universitas di Jepang dengan FSR terendah. Karena kita akan mengolah output poin pertama, disini kami lakukan command awk dengan selection criteria yang sama. Akan tetapi, ditambah dengan command *sort* atau pengurutan berdasarkan kolom fsr-score nya yaitu kolom ke-9 dengan delimiter tanda koma (,).
```
| sort -t, -nk9
```
Setelah di sort, kami akan melakukan print baris pertama yaitu universitas Jepang dengan fsr terendah. Dengan cara menggunakan awk ditambah command NR (number of records) = 1
```
awk 'NR==1{print}'
```

### Langkah Poin 3
Untuk poin ketiga, kita perlu menampilkan 10 universitas dengan rank (ger-rank) tertinggi di jepang. Maka dari itu, akan dilakukan command awk dengan selection criteria if n<10 dan mengandung kata kunci "japan", maka print dan increment n. 
```
awk '{
    if((n<10) && (/Japan)){
        print
        ++n
    }
}' "2023 QS World University Rankings.csv"
```
Jika sudah, hasil tersebut akan di sort berdasarkan kolom ke 20 dengan delimiter (,).
```
| sort -t, -nk20
```

### Langkah Poin 4
Untuk poin keempat, kita perlu mencari universitas paling keren di file csv tersebut. Sehingga, kita hanya perlu melakukan command awk biasa dengan selection criteria pada baris yang mengandung kata kunci keren lalu print barisnya.
```
awk '/Keren/ {print}' "2023 QS World University Rankings.csv"
```
# Penjelasan soal nomor 3
Instruksi untuk membuat sistem registrasi dan login. Registrasi akan dibuat dalam script ``louis.sh`` dan setiap pengguna yang berhasil mendaftar akan disimpan ke dalam file ``/users/user.txt``. Sistem login akan dibuat pada script ``retep.sh``.

### Langkah-langkah
- Membuat File ``log.txt`` yang bertujuan untuk mencatat login dan registrasi dengan command sebagai berikut
```
touch log.txt
```
- Membuat folder users untuk menyimpan file ``user.txt`` yang memiliki informasi username dan password

```
mkdir users
touch users/users.txt
```
- Membuat ``louis.sh`` untuk registrasi username dan password yang telah ditentukan yaitu

```
nano louis.sh
```

Setelah berhasil dibuat, username dan password disimpan ke ``users/users.txt`` dan status registrasi terupdate di log.txt


- Meminta username dan password dari user untuk registrasi
Meminta input username dan password dari pengguna
```
read -p "Masukkan username: " username
read -p "Masukkan password: " password
```
- Membuat variabel time untuk dimasukan ke ``log.txt``

```
time=$(date +"%y/%m/%d %H:%M:%S")
```
- Mengecek apakah username sudah pernah dibuat atau tidak, jika iya maka akan mengirim informasi ke ``log.txt`` bahwa user telah ada. Namun, jika tidak ada, maka akan berlanjut bikin password sesuai dengan kriteria yang telah ditentukan 

1. Minimal 8 karakter
2. Memiliki minimal 1 huruf kapital dan 1 huruf kecil
3. Alphanumeric
4. Tidak boleh sama dengan username 
5. Tidak boleh menggunakan kata chicken atau ernie
```
# Mencari apakah username sudah terdaftar atau belum
if grep -q "^$username:" "users/users.txt"; then
    echo "Username sudah terdaftar."
    echo "$time REGISTER: ERROR User already exists" >> log.txt

else

    # Menentukan kriteria password yang aman
    length=$(echo -n $password | wc -c)
    has_lower=$(echo -n $password | grep -c '[a-z]')
    has_upper=$(echo -n $password | grep -c '[A-Z]')
    has_digit=$(echo -n $password | grep -c '[0-9]')
    is_alphanumeric=$(echo -n $password | grep -c '^[[:alnum:]]*$')
    not_username=$(echo -n $password | grep -c "^$username$")
    contains_chicken=$(echo -n $password | grep -c 'chicken')
    contains_ernie=$(echo -n $password | grep -c 'ernie')

    # Memeriksa apakah password memenuhi kriteria yang ditentukan
    if [ $contains_chicken -eq 1 ] || [ $contains_ernie -eq 1 ]; then
        echo "Password tidak boleh menggunakan kata 'chicken' atau 'ernie'."
    elif [ $not_username -eq 1 ]; then
        echo "Password tidak boleh sama dengan username."
    elif [ $length -lt 8 ]; then
        echo "Password harus minimal 8 karakter."
    elif [ $has_lower -eq 0 ] || [ $has_upper -eq 0 ]; then
        echo "Password harus memiliki minimal 1 huruf kapital dan 1 huruf kecil"
    elif [ $is_alphanumeric -eq 0 ] || [ $has_digit -eq 0 ]; then
        echo "Password harus alphanumeric"
```
- Setelah password berhasil dibuat, maka username dan password akan dimasukan ke `users/users.txt` dan mengirim informasi ke `log.txt` bahwa Registrasi telah berhasil
```
    else
        # Menambahkan username dan password ke dalam file /users/users.txt
        echo "$username:$password" >> users/users.txt
        echo "$time REGISTER: INFO User $username registered successfully" >> log.txt
        echo "Pendaftaran user berhasil!"
    fi
fi
``` 
Setelah file ``louis.sh`` untuk registrasi dibuat, buatlah file ``retep.sh`` untuk login berdasarkan username dan password yang telah dibuat dari file ``louis.sh`` serta ditulis di file ``users/users.txt``.

```
nano retep.sh
```
- Meminta untuk memasukan username dan passowrd yang telah terdaftar dari file ``louis.sh`` dan disetor di ``users/users.txt``
```
# Meminta input username dan password dari pengguna
read -p "Masukkan username: " username
read -p "Masukkan password: " password
```
- Mencari username dan password yang tersimpan dari file ``users/users.txt``
```
# Mencari username dan password pada file /users/users.txt
result=$(grep "^$username:" users/users.txt)
```
- Menentukan waktu saat pencarian username dan password
```
# Menentukan waktu
time=$(date +"%y/%m/%d %H:%M:%S")
```
- Mencari kecocokan antar username serta password dan untuk tiap kondisi status login dimasukan ke log.txt
```
# Memeriksa apakah username ditemukan dan password cocok
if [ -n "$result" ]; then
    stored_password=$(echo $result | cut -d ':' -f 2 | tr -d ' ')
    if [ "$password" = "$stored_password" ]; then
        echo "Selamat datang, $username!"
        echo "$time LOGIN: INFO User $username logged in" >> log.txt
    else
        echo "Password yang dimasukkan salah."
        echo "$time LOGIN: ERROR Failed login attempt on user $username" >> log.txt
    fi
else
    echo "Login gagal: username $username tidak ditemukan."
    echo "$time LOGIN: ERROR Username $username not found" >> log.txt
fi
```
# Penjelasan Soal Nomor 4
Untuk nomor 4, kita perlu melakukan proses backup file log sistem kita dengan syarat log sistem harus dienkripsi terlebih dahulu melalui sistem *cipher*. Cipher disini berarti setiap alfabet pada log sistem akan diubah dengan cara ditambah jam dijalankannya script ``log_encrypt.sh``.

### Langkah Enkripsi
Untuk proses enkripsi system log, pertama-tama kita perlu mengambil jam dan tanggal dijalankannya script untuk disimpan didalam variabel filename sebagai nama file backup nantinya.