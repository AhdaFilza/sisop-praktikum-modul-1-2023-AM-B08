#!/bin/bash
#format penamaan backup
#0 */2 * * * bash ~/sisop/Soal4/log_encrypt.sh
time_date=$(date +"%H:%M %d:%m:%Y")
filename="$time_date.txt"

#simpan seluruh log sistem kedalam variabel syslog
syslog="$(cat /var/log/syslog)"

#alfabet a-z disimpan ke dalam variabel, diulang 2 kali karena ada kemungkinan huruf z ditambah jam 23
normal_abcd=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz
jam=$(date +"%H") #catat jam dilakukannya enkripsi, simpan di variabel
encrypt=$(printf '\n%s' "$syslog" | tr "${normal_abcd:0:26}" "${normal_abcd:${jam}:26}")
jam+=$encrypt #tambah jam ke baris atas hasil enkripsi, agar nanti bisa di dekripsi

#masukkan hasil enkripsi yang sudah disertakan jam ke file backup
echo "$jam" > "$filename" 
